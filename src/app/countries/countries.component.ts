import { Component, OnDestroy, OnInit } from '@angular/core';
import { Country } from '../shared/country.model';
import { Subscription } from 'rxjs';
import { CountryService } from '../shared/country.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit, OnDestroy {
  countries!: Country[];
  countriesChangeSubscription!: Subscription;
  countriesFetchSubscription!: Subscription;
  loading: boolean = false;

  constructor(
    private countryService: CountryService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.countries = this.countryService.getCountries();
    this.countriesChangeSubscription = this.countryService.countriesChange.subscribe((countries: Country[]) =>{
      this.countries = countries;
    });
    this.countriesFetchSubscription = this.countryService.countriesFetching.subscribe((isFetching: boolean) =>{
      this.loading = isFetching;
    });
    this.countryService.fetchCountries();
  }

  ngOnDestroy(): void {
    this.countriesChangeSubscription.unsubscribe();
    this.countriesFetchSubscription.unsubscribe();
  }
}
