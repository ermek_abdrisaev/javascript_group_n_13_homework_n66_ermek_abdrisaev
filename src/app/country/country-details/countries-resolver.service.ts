import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Country } from '../../shared/country.model';
import { Observable } from 'rxjs';
import { CountryService } from '../../shared/country.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountriesResolverService implements Resolve<Country>{

  constructor(private countryService: CountryService, private http: HttpClient) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country> {
    const alphaCode = <string>route.params['id'];
    return this.countryService.fetchCountry(alphaCode);
  }
}
