import { Component, OnInit } from '@angular/core';
import { Country } from '../../shared/country.model';
import { CountryService } from '../../shared/country.service';
import { ActivatedRoute, Data} from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit{
  country!: Country;
  private subscription!: Subscription;

  constructor(
    private countryService: CountryService,
    private route: ActivatedRoute,
    ) {}

  ngOnInit(): void {
    this.route.data.subscribe((data: Data) =>{
      this.country = <Country>data.country;
    });
  }


}
