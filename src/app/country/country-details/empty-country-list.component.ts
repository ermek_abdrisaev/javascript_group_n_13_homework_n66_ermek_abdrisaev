import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-country-list',
  template: `
    <h4>Country details</h4>
    <p>No countries to show</p>
  `
})
export class EmptyCountryListComponent {}
