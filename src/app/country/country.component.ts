import { Component, Input, OnInit } from '@angular/core';
import { Country } from '../shared/country.model';
import { CountryService } from '../shared/country.service';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {
  @Input() country!: Country;

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
  }

}
