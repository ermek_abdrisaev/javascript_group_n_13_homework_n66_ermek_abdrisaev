import { HttpClient } from '@angular/common/http';
import { Country } from './country.model';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

@Injectable()

export class CountryService {
  countriesChange = new Subject<Country[]>();
  countriesFetching = new Subject<boolean>();
  countryFetching = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  private countries: Country[] = [];

  fetchCountries() {
    this.countriesFetching.next(true);

    this.http.get<{ [id: string]: Country }>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const countryData = result[id];
          return new Country(countryData.name, countryData.alpha3Code, countryData.capital, countryData.population, countryData.region);
        });
      }))
      .subscribe(countries => {
        this.countries = countries;
        this.countriesChange.next(this.countries.slice());
        this.countriesFetching.next(false);
      }, error => {
        this.countriesFetching.next(false);
      });
  }

  getCountries() {
    return this.countries.slice();
  }

  fetchCountry(id: string) {
    this.countryFetching.next(true);

    return this.http.get<Country>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${id}`)
      .pipe(map(result => {
        return new Country(result.name, result.alpha3Code, result.capital, result.population, result.region);
      }));
  }
}

