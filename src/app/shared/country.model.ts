export class Country {
  constructor(
    public name: string,
    public alpha3Code: string,
    public capital: string,
    public population: number,
    public region: string,
  ){}
}
