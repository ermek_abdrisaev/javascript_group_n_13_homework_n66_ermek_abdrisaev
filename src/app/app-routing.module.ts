import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found.component';
import { EmptyCountryListComponent } from './country/country-details/empty-country-list.component';
import { CountryDetailsComponent } from './country/country-details/country-details.component';
import { CountriesResolverService } from './country/country-details/countries-resolver.service';

const routes: Routes = [
  {path: 'countries', component: HomeComponent},
  {path: '', component: EmptyCountryListComponent},
  {path: ':id', component: CountryDetailsComponent,
  resolve: {
    country: CountriesResolverService
  }},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
